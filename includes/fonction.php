<?php

header('Content-Type: text/html; charset=ISO-8859-1');

try
{
	//$bdd = new PDO('mysql:host=localhost;dbname=endworld', 'xdidx', 'k10h25');
	$bdd = new PDO('mysql:host=localhost;dbname=endworld', 'root', '');
}
catch(Exception $e)
{
		die('Erreur : '.$e->getMessage());
}

$timestamp = time() + 7200; 
$date = 'Le '.date('d\/m\/Y \� H:i', $timestamp);

session_start();

if(isset($_GET['deco'])){
	$_SESSION['login'] = "-1";
	$_SESSION['pseudo'] = "";
	$req = $bdd->prepare('UPDATE online set id = ? WHERE ip = ?');
	$req->execute(array("0", $_SERVER['REMOTE_ADDR']));
}
/*
///////////VISITES//////////
if(!isset($_SESSION['visite']) OR $_SESSION['visite']<1){
	$_SESSION['visite'] = 1;
	if(!isset($_SESSION['login']) OR $_SESSION['login']<1){
		$_SESSION['pseudo'] = "Visiteur";
	}
		
	$req = $bdd->prepare('INSERT INTO visites(ip, pseudo, date) values(?, ?, ?)');
	$req->execute(array($_SERVER['REMOTE_ADDR'], $_SESSION['pseudo'], $date));
	
	$reponse = $bdd->query('SELECT * FROM visites WHERE date="'.$date.'" AND ip="'.$_SERVER['REMOTE_ADDR'].'"') or die(mysql_error());
	$donnees = $reponse->fetch();
	$_SESSION['visite'] = $donnees['id'];
}

///////////ONLINE//////////
$ip = $_SERVER['REMOTE_ADDR'];
if(isset($_SESSION['login']) AND $_SESSION['login'] > 0){
	$id = $_SESSION['pseudo'];
}else{
	$id = 0;
}
*/

function code($texte)
{
	$dir_smileys='images/smiley/';
	
	$texte = str_replace('<div>', '', $texte);
	$texte = str_replace('<span>', '', $texte);	
	$texte = str_replace('</div>', '', $texte);
	$texte = str_replace('</span>', '', $texte);

	$texte = str_replace(':D', '<img src="'.$dir_smileys.'heureux.gif" title="heureux" alt="heureux" />', $texte);
	$texte = str_replace('^^', '<img src="'.$dir_smileys.'trema.gif" title="trema" alt="trema" />', $texte);
	$texte = str_replace(':lol:', '<img src="'.$dir_smileys.'lol.gif" title="lol" alt="lol" />', $texte);
	$texte = str_replace(':(', '<img src="'.$dir_smileys.'triste.gif" title="triste" alt="triste" />', $texte);
	$texte = str_replace(':frime:', '<img src="'.$dir_smileys.'frime.gif" title="frime" alt="frime" />', $texte);
	$texte = str_replace(':)', '<img src="'.$dir_smileys.'sourire.gif" title="sourire" alt="sourire" />', $texte);
	$texte = str_replace(':rire:', '<img src="'.$dir_smileys.'rire.gif" title="rire" alt="rire" />', $texte);
	$texte = str_replace(':s', '<img src="'.$dir_smileys.'confus.gif" title="confus" alt="confus" />', $texte);
	$texte = str_replace(':O', '<img src="'.$dir_smileys.'choc.gif" title="choc" alt="choc" />', $texte);
	$texte = str_replace(':question:', '<img src="'.$dir_smileys.'question.gif" title="?" alt="?" />', $texte);
	$texte = str_replace(':exclamation:', '<img src="'.$dir_smileys.'exclamation.gif" title="!" alt="!" />', $texte);
	$texte = str_replace('Oo', '<img src="'.$dir_smileys.'Oo.gif" title="Oo" alt="Oo" />', $texte);
	$texte = str_replace(':@', '<img src="'.$dir_smileys.'colere.gif" title="colere" alt="colere" />', $texte);
	$texte = str_replace(':help:', '<img src="'.$dir_smileys.'help.gif" title="help" alt="help" />', $texte);
	$texte = str_replace(':interdit:', '<img src="'.$dir_smileys.'interdit.gif" title="interdit" alt="interdit" />', $texte);
	$texte = str_replace(':reglement:', '<img src="'.$dir_smileys.'reglement.gif" title="reglement" alt="reglement" />', $texte);
	$texte = str_replace(':heu:', '<img src="'.$dir_smileys.'heu.gif" title="heu" alt="heu" />', $texte);
	$texte = str_replace(';)', '<img src="'.$dir_smileys.'tkt.gif" title="tkt" alt="tkt" />', $texte);
	$texte = str_replace(':language:', '<img src="'.$dir_smileys.'sms.gif" title="sms" alt="sms" />', $texte);
	

	// Bloc des balises [url]-[/url]
	$texte = preg_replace('`\[url=([http://].+?)](.+?)\[/url]`si','<a href="#" onclick="window.open(\'$1\',\'popup\');" title="$1">$2</a>',$texte); 
	$texte = preg_replace('`\[url=(.+?)](.+?)\[/url]`si','<a href="#" onclick="window.open(\'http://$1\',\'popup\');" title="$1">$2</a>',$texte); 
	$texte = preg_replace('`\[url]([http://].+?)\[/url]`si','<a href="#" onclick="window.open(\'$1\',\'popup\');" title="$1">$1</a>',$texte); 
	$texte = preg_replace('`\[url](.+?)\[/url]`si','<a href="#" onclick="window.open(\'http://$1\',\'popup\');" title="$1">$1</a>',$texte); 
	     
	// Bloc des balises [b]-[/b]
	$texte = preg_replace('#\[b](.+?)\[/b]#si','<strong>$1</strong>',$texte); 
	
	// Bloc des balises [u]-[/u]
	$texte = preg_replace('#\[u](.+?)\[/u]#si','<span style="text-decoration:underline;">$1</span>',$texte);
	
	// Bloc des balises [i]-[/i]
	$texte = preg_replace('#\[i](.+?)\[/i]#si','<em>$1</em>',$texte);
	
	// Bloc des balises [strike]-[/strike]
	$texte = preg_replace('#\[strike](.+?)\[/strike]#si','<span style="text-decoration:line-through;">$1</span>',$texte); 
	
	// Bloc des balises [overline]-[/overline]
	$texte = preg_replace('#\[overline](.+?)\[/overline]#si','<span style="text-decoration:overline;">$1</span>',$texte); 
	
	/* Bloc des balises [quote]-[/quote]
	$texte = preg_replace('#\[quote=me](.+?)\[/quote]#si','<br /><strong>J\'ai �crit</strong> :<br/><div class="quote">$1 </div><br />',$texte); 
	$texte = preg_replace('#\[quote=(.+?)](.+?)\[/quote]#si','<br /><strong>$1 a �crit</strong> :<br/><div class="quote">$2 </div><br />',$texte); 
	$texte = preg_replace('#\[quote](.+?)\[/quote]#si','<br /><strong>Citation</strong> :<br/><div class="quote">$1 </div><br />',$texte); 
	*/
	
	// Bloc des balises [img]-[/img]
	$texte = preg_replace('#\[img=(.+?)](.+?)\[/img]#si','<a href="$1"><div class="img01"><img class="imageforum" src="$1" alt="$2"/></div></a>',$texte);
	$texte = preg_replace('#\[img](.+?)\[/img]#si','<a href="$1"><div class="img01"><img class="imageforum" src="$1" /></div></a>',$texte);
	
	// Bloc des balises [mail]-[/mail]
	$texte = preg_replace('#\[mail=([mailto:].+?)](.+?)\[/mail]#si','<a href="$1">$2</a>',$texte); 
	$texte = preg_replace('#\[mail=(.+?)](.+?)\[/mail]#si','<a href="mailto:$1">$2</a>',$texte); 
	$texte = preg_replace('#\[mail]([mailto:].+?)\[/mail]#si','<a href="$1">$1</a>',$texte); 
	$texte = preg_replace('#\[mail](.+?)\[/mail]#si','<a href="mailto:$1">$1</a>',$texte); 
	
	// Bloc des balises [align]-[/align]
	$texte = preg_replace('#\[align=(left|center|right)](.+?)\[/align]#si','<div style="text-align:$1; width:100%;">$2</div>',$texte); 
	
	// Bloc des balises [color]-[/color]
	$texte = preg_replace('#\[color=(.+?)](.+?)\[/color]#si','<span style="color:$1;">$2</span>',$texte); 
	
	// Bloc des balises [size]-[/size]
	$texte = preg_replace('#\[size=([0-6]{0,1}[0-9]{1})](.+?)\[/size]#si','<span style="font-size:$1px;">$2</span>',$texte); 
	
	// Bloc des balises [size]-[/size] trop grandes
	$texte = preg_replace('#\[size=([7-9]{1}[0-9]{1,})](.+?)\[/size]#si','$2',$texte); 
	
	// Bloc des balises [thick]-[/thick]
	$texte = preg_replace('#\[thick=([0-9]{1,3})](.+?)\[/thick]#si','<span style="font-weight:$1px;">$2</span>',$texte); 
	
	// Bloc des balises [style]-[/style]
	$texte = preg_replace('#\[style=(normal|italique|oblique)](.+?)\[/style]#si','<span style="font-style:$1;">$2</span>',$texte); 
	
	// Bloc des balises [weight]-[/weight]
	$texte = preg_replace('#\[weight=(lighter|bold|bolder)](.+?)\[/weight]#si','<span style="font-weight:$1;">$2</span>',$texte); 
	
	// Bloc des balises [decoration]-[/decoration]
	$texte = preg_replace('#\[decoration=(underline|line-through|overline|blink)](.+?)\[/decoration]#si','<span style="font-weight:$1;">$2</span>',$texte); 
	
	// Bloc des balises [font]-[/font]
	$texte = preg_replace('#\[font=(.+?)](.+?)\[/font]#si','<span style="font-family:$1;">$2</span>',$texte);

	// Bloc des balises [list]-[/list]
	$texte = preg_replace('`\[list=(circle|disc|square|i)](.+?)\[/list]`si','<ul type="$1">$2</ul>',$texte);
	$texte = preg_replace('`\[list](.+?)\|/list]`si','<ul>$1</ul>',$texte);
	
	// Bloc des balises [*]
	$texte = preg_replace('`\[\*=(circle|disc|square|i)](.+?)`si','<li type="$1">$2',$texte);
	$texte = preg_replace('`\[\*](.+?)`si','<li>$1',$texte);

	//Bloc des balises [spoil]-[/spoil]
	$texte = preg_replace('`\[spoil\](.+)\[/spoil\]`isU', '<span class="spoilertexte">Texte cach� : cliquez sur le cadre pour l\'afficher</span><div class="spoiler" onclick="switch_spoiler(this)" ><div style="display:none;" class="spoiler3">$1</div></div>', $texte); 
																																																													
																																																													
return $texte;																																																											
}																																																												
																																																																	
function code_chat($texte)
{
	$dir_smileys='../images/smiley/';
	
	$texte = str_replace('<div>', '', $texte);
	$texte = str_replace('<span>', '', $texte);	
	$texte = str_replace('</div>', '', $texte);
	$texte = str_replace('</span>', '', $texte);

	$texte = str_replace(':D', '<img src="'.$dir_smileys.'heureux.gif" title="heureux" alt="heureux" />', $texte);
	$texte = str_replace('^^', '<img src="'.$dir_smileys.'trema.gif" title="trema" alt="trema" />', $texte);
	$texte = str_replace(':lol:', '<img src="'.$dir_smileys.'lol.gif" title="lol" alt="lol" />', $texte);
	$texte = str_replace(':(', '<img src="'.$dir_smileys.'triste.gif" title="triste" alt="triste" />', $texte);
	$texte = str_replace(':frime:', '<img src="'.$dir_smileys.'frime.gif" title="frime" alt="frime" />', $texte);
	$texte = str_replace(':)', '<img src="'.$dir_smileys.'sourire.gif" title="sourire" alt="sourire" />', $texte);
	$texte = str_replace(':rire:', '<img src="'.$dir_smileys.'rire.gif" title="rire" alt="rire" />', $texte);
	$texte = str_replace(':s', '<img src="'.$dir_smileys.'confus.gif" title="confus" alt="confus" />', $texte);
	$texte = str_replace(':O', '<img src="'.$dir_smileys.'choc.gif" title="choc" alt="choc" />', $texte);
	$texte = str_replace(':question:', '<img src="'.$dir_smileys.'question.gif" title="?" alt="?" />', $texte);
	$texte = str_replace(':exclamation:', '<img src="'.$dir_smileys.'exclamation.gif" title="!" alt="!" />', $texte);
	$texte = str_replace('Oo', '<img src="'.$dir_smileys.'Oo.gif" title="Oo" alt="Oo" />', $texte);
	$texte = str_replace(':@', '<img src="'.$dir_smileys.'colere.gif" title="colere" alt="colere" />', $texte);
	$texte = str_replace(':help:', '<img src="'.$dir_smileys.'help.gif" title="help" alt="help" />', $texte);
	$texte = str_replace(':interdit:', '<img src="'.$dir_smileys.'interdit.gif" title="interdit" alt="interdit" />', $texte);
	$texte = str_replace(':reglement:', '<img src="'.$dir_smileys.'reglement.gif" title="reglement" alt="reglement" />', $texte);
	$texte = str_replace(':heu:', '<img src="'.$dir_smileys.'heu.gif" title="heu" alt="heu" />', $texte);
	$texte = str_replace(';)', '<img src="'.$dir_smileys.'tkt.gif" title="tkt" alt="tkt" />', $texte);
	$texte = str_replace(':language:', '<img src="'.$dir_smileys.'sms.gif" title="sms" alt="sms" />', $texte);
	

	// Bloc des balises [url]-[/url]
	$texte = preg_replace('`\[url=([http://].+?)](.+?)\[/url]`si','<a href="#" onclick="window.open(\'$1\',\'popup\');" title="$1">$2</a>',$texte); 
	$texte = preg_replace('`\[url=(.+?)](.+?)\[/url]`si','<a href="#" onclick="window.open(\'http://$1\',\'popup\');" title="$1">$2</a>',$texte); 
	$texte = preg_replace('`\[url]([http://].+?)\[/url]`si','<a href="#" onclick="window.open(\'$1\',\'popup\');" title="$1">$1</a>',$texte); 
	$texte = preg_replace('`\[url](.+?)\[/url]`si','<a href="#" onclick="window.open(\'http://$1\',\'popup\');" title="$1">$1</a>',$texte); 
	     
	// Bloc des balises [b]-[/b]
	$texte = preg_replace('#\[b](.+?)\[/b]#si','<strong>$1</strong>',$texte); 
	
	// Bloc des balises [u]-[/u]
	$texte = preg_replace('#\[u](.+?)\[/u]#si','<span style="text-decoration:underline;">$1</span>',$texte);
	
	// Bloc des balises [i]-[/i]
	$texte = preg_replace('#\[i](.+?)\[/i]#si','<em>$1</em>',$texte);
	
	// Bloc des balises [strike]-[/strike]
	$texte = preg_replace('#\[strike](.+?)\[/strike]#si','<span style="text-decoration:line-through;">$1</span>',$texte); 
	
	// Bloc des balises [overline]-[/overline]
	$texte = preg_replace('#\[overline](.+?)\[/overline]#si','<span style="text-decoration:overline;">$1</span>',$texte); 
	
	// Bloc des balises [mail]-[/mail]
	$texte = preg_replace('#\[mail=([mailto:].+?)](.+?)\[/mail]#si','<a href="$1">$2</a>',$texte); 
	$texte = preg_replace('#\[mail=(.+?)](.+?)\[/mail]#si','<a href="mailto:$1">$2</a>',$texte); 
	$texte = preg_replace('#\[mail]([mailto:].+?)\[/mail]#si','<a href="$1">$1</a>',$texte); 
	$texte = preg_replace('#\[mail](.+?)\[/mail]#si','<a href="mailto:$1">$1</a>',$texte); 
	
	// Bloc des balises [align]-[/align]
	$texte = preg_replace('#\[align=(left|center|right)](.+?)\[/align]#si','<div style="text-align:$1; width:100%;">$2</div>',$texte); 
	
	// Bloc des balises [color]-[/color]
	$texte = preg_replace('#\[color=(.+?)](.+?)\[/color]#si','<span style="color:$1;">$2</span>',$texte); 
	
	// Bloc des balises [thick]-[/thick]
	$texte = preg_replace('#\[thick=([0-9]{1,3})](.+?)\[/thick]#si','<span style="font-weight:$1px;">$2</span>',$texte); 
	
	// Bloc des balises [style]-[/style]
	$texte = preg_replace('#\[style=(normal|italique|oblique)](.+?)\[/style]#si','<span style="font-style:$1;">$2</span>',$texte); 
	
	// Bloc des balises [weight]-[/weight]
	$texte = preg_replace('#\[weight=(lighter|bold|bolder)](.+?)\[/weight]#si','<span style="font-weight:$1;">$2</span>',$texte); 
	
	// Bloc des balises [decoration]-[/decoration]
	$texte = preg_replace('#\[decoration=(underline|line-through|overline|blink)](.+?)\[/decoration]#si','<span style="font-weight:$1;">$2</span>',$texte); 
	
	// Bloc des balises [font]-[/font]
	$texte = preg_replace('#\[font=(.+?)](.+?)\[/font]#si','<span style="font-family:$1;">$2</span>',$texte);

	// Bloc des balises [list]-[/list]
	$texte = preg_replace('`\[list=(circle|disc|square|i)](.+?)\[/list]`si','<ul type="$1">$2</ul>',$texte);
	$texte = preg_replace('`\[list](.+?)\|/list]`si','<ul>$1</ul>',$texte);
	
	// Bloc des balises [*]
	$texte = preg_replace('`\[\*=(circle|disc|square|i)](.+?)`si','<li type="$1">$2',$texte);
	$texte = preg_replace('`\[\*](.+?)`si','<li>$1',$texte);

	//Bloc des balises [spoil]-[/spoil]
	$texte = preg_replace('`\[spoil\](.+)\[/spoil\]`isU', '<span class="spoilertexte">Texte cach� : cliquez sur le cadre pour l\'afficher</span><div class="spoiler" onclick="switch_spoiler(this)"><div style="visibility: hidden;" class="spoiler3">$1</div></div>', $texte); 

return $texte;
}

function Tronquer_Texte($texte, $longeur_max)
{
    if (strlen($texte) > $longeur_max){
    $texte = substr($texte, 0, $longeur_max);

    $dernier_espace = strrpos($texte, " ");

    $texte = substr($texte, 0, $dernier_espace)."...";

    }
    return $texte;
}

function move_image($file, $nom)
{
    $extension_upload = strtolower(substr(  strrchr($file['name'], '.')  ,1));
    $name = "images/".$nom.".".$extension_upload;
    move_uploaded_file($file['tmp_name'],$name);
}

?>
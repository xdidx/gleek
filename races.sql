-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Jeu 14 Juillet 2011 à 01:26
-- Version du serveur: 5.5.8
-- Version de PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `endworld`
--

-- --------------------------------------------------------

--
-- Structure de la table `races`
--

CREATE TABLE IF NOT EXISTS `races` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `races`
--

INSERT INTO `races` (`id`, `nom`, `description`) VALUES
(1, 'Tikor', 'Les Tikor sont les descendants des humains, mais sont beaucoup plus petit (1m20 pour les plus grands). Le nom Tikor provient de la langue humaine : c''est la contraction des mots "petit" et "corps". Ils ne sont pas très puissant mais étant très malin et habile ils peuvent facilement se sortir des situations les plus difficiles. Ils utilisent des armes légères comme des hachettes, des dagues et des petits bâtons. Ils sont débutant en magie blanche et noire.'),
(2, 'Snuks', 'Les Snuks sont des bêtes féroces. Pour les Tikor ceux sont des géants assoiffés de sang. Ils ont une force phénoménale mais leur vitesse de frappe est très lente. Leur intelligence qui est presque inexistante les empêche d''utiliser toute sorte de magie. Ils utilisent de grandes massues, marteaux ou alors leurs poings pour défigurer leurs ennemies. Mieux vaut ne pas se frotter a un Snuks !  '),
(3, 'Kenshi', 'Kenshi'),
(4, 'Wara', 'Wara'),
(5, 'Troïns', 'Troïns');

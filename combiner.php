<?php

if(isset($_GET['sprite'])){

header ("Content-type: image/png");
$race_ensemble = $_GET['sprite'];
$race_ensemble = explode(',', $race_ensemble);
		
$race = $race_ensemble[0];
if(isset($_GET['menu'])){
	$destination = imagecreatefrompng("images/races/".$race."/apercu.png"); // La photo est la destination
}else{
	$destination = imagecreatefrompng("images/races/".$race."/sprite.png"); // La photo est la destination
}
///////////////////////////CHEVEUX/////////////////////////
$cheveux = explode('.', $race_ensemble[1]);
$image_cheveux = imagecreatefrompng("images/races/items/cheveux/".$cheveux[0].".png");

$largeur_source = imagesx($image_cheveux);
$hauteur_source = imagesy($image_cheveux);

for($i=0;$i<imagecolorstotal($image_cheveux);$i = $i+1) { 
	$col=ImageColorsForIndex($image_cheveux,$i); 
	if($col['red'] == 0 AND $col['green'] == 255 AND $col['blue'] == 255){
		imagecolorset($image_cheveux,$i,$cheveux[1],$cheveux[2],$cheveux[3]);
	}	
}

imagecopymerge($destination, $image_cheveux, 0, 0, 0, 0, $largeur_source, $hauteur_source, 100);

/*///////////////////////////VISAGE/////////////////////////
$visage = explode('.', $race_ensemble[2]);
$image_visage = imagecreatefrompng("images/races/items/visage/".$visage[0].".png");

$largeur_source = imagesx($image_visage);
$hauteur_source = imagesy($image_visage);

for($i=0;$i<imagecolorstotal($image_visage);$i = $i+1) { 
	$col=ImageColorsForIndex($image_visage,$i); 

	$red_set=($visage[1]+$col['red']);  
	$green_set=($visage[2]+$col['green']); 
	$blue_set=($visage[3]+$col['blue']); 
	imagecolorset($image_visage,$i,$red_set,$green_set,$blue_set);
}

imagecopymerge($destination, $image_visage, 0, 0, 0, 0, $largeur_source, $hauteur_source, 100);*/


///////////////////////////TORSE/////////////////////////
$torse = explode('.', $race_ensemble[3]);
$image_torse = imagecreatefrompng("images/races/items/torse/".$torse[0].".png");

$largeur_source = imagesx($image_torse);
$hauteur_source = imagesy($image_torse);

for($i=0;$i<imagecolorstotal($image_torse);$i = $i+1) { 
	$col=ImageColorsForIndex($image_torse,$i); 

	if($col['red'] == 0 AND $col['green'] == 255 AND $col['blue'] == 255){
		imagecolorset($image_torse,$i,$torse[1],$torse[2],$torse[3]);
	}
}

imagecopymerge($destination, $image_torse, 0, 0, 0, 0, $largeur_source, $hauteur_source, 100);



///////////////////////////JAMBES/////////////////////////
$jambes = explode('.', $race_ensemble[4]);
$image_jambes = imagecreatefrompng("images/races/items/jambes/".$jambes[0].".png");

$largeur_source = imagesx($image_jambes);
$hauteur_source = imagesy($image_jambes);

for($i=0;$i<imagecolorstotal($image_jambes);$i = $i+1) { 
	$col=ImageColorsForIndex($image_jambes,$i); 

	if($col['red'] == 0 AND $col['green'] == 255 AND $col['blue'] == 255){
		imagecolorset($image_jambes,$i,$jambes[1],$jambes[2],$jambes[3]);
	}
}

imagecopymerge($destination, $image_jambes, 0, 0, 0, 0, $largeur_source, $hauteur_source, 100);


///////////////////////////PIEDS/////////////////////////
$pieds = explode('.', $race_ensemble[5]);
$image_pieds = imagecreatefrompng("images/races/items/pieds/".$pieds[0].".png");

$largeur_source = imagesx($image_pieds);
$hauteur_source = imagesy($image_pieds);

for($i=0;$i<imagecolorstotal($image_pieds);$i = $i+1) { 
	$col=ImageColorsForIndex($image_pieds,$i); 

	if($col['red'] == 0 AND $col['green'] == 255 AND $col['blue'] == 255){
		imagecolorset($image_pieds,$i,$pieds[1],$pieds[2],$pieds[3]);
	}
}

imagecopymerge($destination, $image_pieds, 0, 0, 0, 0, $largeur_source, $hauteur_source, 100);

///////////////////////////CHEVEUX/////////////////////////
$cheveux = explode('.', $race_ensemble[1]);
$image_cheveux = imagecreatefrompng("images/races/items/cheveux/".$cheveux[0].".png");

$largeur_source = imagesx($image_cheveux);
$hauteur_source = imagesy($image_cheveux);

for($i=0;$i<imagecolorstotal($image_cheveux);$i = $i+1) { 
	$col=ImageColorsForIndex($image_cheveux,$i); 
	if($col['red'] == 0 AND $col['green'] == 255 AND $col['blue'] == 255){
		imagecolorset($image_cheveux,$i,$cheveux[1],$cheveux[2],$cheveux[3]);
	}	
}

imagecopymerge($destination, $image_cheveux, 0, 0, 0, 0, $largeur_source, $hauteur_source, 100);

///////////////////////////TRANSPARENCE/////////////////////////
$fond = imagecolorallocate($destination, 0, 255, 0);
imagecolortransparent($destination, $fond);

imagepng($destination);

}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////APERCU//////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

else if(isset($_GET['apercu']) AND isset($_GET['pos'])){

header ("Content-type: image/png");
$race_ensemble = $_GET['apercu'];
$race_ensemble = explode(',', $race_ensemble);
		
$race = $race_ensemble[0];
$destination = imagecreatefrompng("images/races/".$race."/apercu_".$_GET['pos'].".png"); // La photo est la destination

$decalage =0;

if($_GET['pos'] == "gauche"){
	$decalage = 48;
}elseif($_GET['pos'] == "droite"){
	$decalage = 96;
}elseif($_GET['pos'] == "dos"){
	$decalage = 144;
}




///////////////////////////VISAGE/////////////////////////
/*$visage = explode('.', $race_ensemble[2]);
$image_visage = imagecreatefrompng("images/races/items/visage/".$visage[0].".png");

$largeur_source = imagesx($image_visage);
$hauteur_source = imagesy($image_visage);

for($i=0;$i<imagecolorstotal($image_visage);$i = $i+1) { 
	$col=ImageColorsForIndex($image_visage,$i); 

	$red_set=($visage[1]+$col['red']);  
	$green_set=($visage[2]+$col['green']); 
	$blue_set=($visage[3]+$col['blue']); 
	imagecolorset($image_visage,$i,$red_set,$green_set,$blue_set);
}

imagecopymerge($destination, $image_visage, 0, 0, 0, 0, $largeur_source, $hauteur_source, 100);
*/

///////////////////////////TORSE/////////////////////////
$torse = explode('.', $race_ensemble[3]);
$image_torse = imagecreatefrompng("images/races/items/torse/".$torse[0].".png");

$largeur_source = imagesx($image_torse);
$hauteur_source = imagesy($image_torse);

for($i=0;$i<imagecolorstotal($image_torse);$i = $i+1) {
	$col=ImageColorsForIndex($image_torse,$i); 

	if($col['red'] == 0 AND $col['green'] == 255 AND $col['blue'] == 255){
		imagecolorset($image_torse,$i,$torse[1],$torse[2],$torse[3]);
	}
}

imagecopymerge($destination, $image_torse, 0, 0, 0, $decalage, $largeur_source, $hauteur_source, 100);



///////////////////////////JAMBES/////////////////////////
$jambes = explode('.', $race_ensemble[4]);
$image_jambes= imagecreatefrompng("images/races/items/jambes/".$jambes[0].".png");

$largeur_source = imagesx($image_jambes);
$hauteur_source = imagesy($image_jambes);

for($i=0;$i<imagecolorstotal($image_jambes);$i = $i+1) { 
	$col=ImageColorsForIndex($image_jambes,$i); 

	if($col['red'] == 0 AND $col['green'] == 255 AND $col['blue'] == 255){
		imagecolorset($image_jambes,$i,$jambes[1],$jambes[2],$jambes[3]);
	}
}

imagecopymerge($destination, $image_jambes, 0, 0, 0, $decalage, $largeur_source, $hauteur_source, 100);


///////////////////////////PIEDS/////////////////////////
$pieds = explode('.', $race_ensemble[5]);
$image_pieds = imagecreatefrompng("images/races/items/pieds/".$pieds[0].".png");

$largeur_source = imagesx($image_pieds);
$hauteur_source = imagesy($image_pieds);

for($i=0;$i<imagecolorstotal($image_pieds);$i = $i+1) { 
	$col=ImageColorsForIndex($image_pieds,$i); 

	if($col['red'] == 0 AND $col['green'] == 255 AND $col['blue'] == 255){
		imagecolorset($image_pieds,$i,$pieds[1],$pieds[2],$pieds[3]);
	}
}

///////////////////////////CHEVEUX/////////////////////////
$cheveux = explode('.', $race_ensemble[1]);
$image_cheveux = imagecreatefrompng("images/races/items/cheveux/".$cheveux[0].".png");

$largeur_source = imagesx($image_cheveux);
$hauteur_source = imagesy($image_cheveux);

for($i=0;$i<imagecolorstotal($image_cheveux);$i = $i+1) { 
	$col=ImageColorsForIndex($image_cheveux,$i); 

	if($col['red'] == 0 AND $col['green'] == 255 AND $col['blue'] == 255){
		imagecolorset($image_cheveux,$i,$cheveux[1],$cheveux[2],$cheveux[3]);
	}
}

imagecopymerge($destination, $image_cheveux, 0, 0, 0, $decalage, $largeur_source, $hauteur_source, 100);

imagecopymerge($destination, $image_pieds, 0, 0, 0, $decalage, $largeur_source, $hauteur_source, 100);

///////////////////////////TRANSPARENCE/////////////////////////
$fond = imagecolorallocate($destination, 0, 255, 0);
imagecolortransparent($destination, $fond);

imagepng($destination);
}

?>

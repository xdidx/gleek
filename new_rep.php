<?php
include('includes/fonction.php');

if(isset($_SESSION['login']) AND $_SESSION['login'] >= 1){

if(isset($_GET['vide'])){
	echo '<span class="alerte">Veuillez remplir tous les champs</span>';
}


//REPONSE MESSAGE
if (isset($_POST['message_sujet']) AND isset($_POST['forum_reponse'])) {

	$req = $bdd->prepare('INSERT INTO messages(message, auteur, date, sujet, forum) VALUES(?, ?, ?, ?, ?)');
	$req->execute(array(htmlspecialchars($_POST['message_sujet']), $_SESSION['pseudo'], $date, $_POST['sujet_reponse'], $_POST['forum_reponse']));
	
	$req = $bdd->prepare('UPDATE sujets SET derniere_date = ? WHERE id = ?');
	$req->execute(array(time(), $_POST['sujet_reponse']));
	
	header('Location: sujets.php?sujet='.$_POST['sujet_reponse'].'&succes');
	
}


if (isset($_POST['message_sujet']) AND !isset($_POST['forum_reponse'])){
	$req = $bdd->prepare('UPDATE messages SET message = ? WHERE id = ?');
	$req->execute(array(htmlspecialchars($_POST['message_sujet']), $_POST['id_message_modif']));
	header('Location: sujets.php?sujet='.$_POST['sujet'].'&succes');
}


//EDITION MESSAGE
if (isset($_POST['id_message'])) {

	
$titre = "Reponse";
include('includes/corps_haut.php');

$reponse = $bdd->query('SELECT * FROM messages WHERE id="'.$_POST['id_message'].'"') or die(mysql_error());
$donnees = $reponse->fetch();
?>

	<div class="new_sujet">
		<FORM id="forum" name="formulaire" method=post action="new_rep.php" style="text-align:center;">
			<table >
				<tr>
					<td class="creer_gauche">
						Mise en page :
					</td>
					<td class="creer_droit">
						<input type="image" src="./images/gras.gif" id="gras" name="gras" value="Gras" onClick="javascript:bbcode('[b]', '[/b]', 'message_sujet');return(false)" />
						<input type="image" src="./images/italic.gif" id="italic" name="italic" value="Italic" onClick="javascript:bbcode('[i]', '[/i]', 'message_sujet');return(false)" />
						<input type="image" src="./images/underligne.gif" id="soulign�" name="soulign�" value="Soulign�" onClick="javascript:bbcode('[u]', '[/u]', 'message_sujet');return(false)" />
						<input type="image" src="./images/strike.gif" id="barrer" name="barrer" value="barrer" onClick="javascript:bbcode('[strike]', '[/strike]', 'message_sujet');return(false)" />
						<input type="image" src="./images/color.gif" value="bleu" onClick="javascript:bbcode('[color=blue]', '[/color]', 'message_sujet');return(false)" />
						<input type="image" src="./images/size.gif" value="grand" onClick="javascript:bbcode('[size=20]', '[/size]', 'message_sujet');return(false)" />
						<input type="image" src="./images/quote.gif" value="citer" onClick="javascript:bbcode('[quote]', '[/quote]', 'message_sujet');return(false)" />
						<input type="image" src="./images/spoil.gif" value="spoil" onClick="javascript:bbcode('[spoil]', '[/spoil]', 'message_sujet');return(false)" />
						<input type="image" src="./images/lien.gif" id="lien" name="lien" value="Lien" onClick="javascript:bbcode('[url]', '[/url]', 'message_sujet');return(false)" />
						<input type="image" src="./images/img.gif" id="img" name="img" value="image" onClick="javascript:bbcode('[img]', '[/img]', 'message_sujet');return(false)" />
						<br/>
						<input type="image" src="./images/left.gif" id="left" name="left" value="aligner � gauche" onClick="javascript:bbcode('[align=left]', '[/align]', 'message_sujet');return(false)" />
						<input type="image" src="./images/center.gif" id="center" name="center" value="aligner au centre" onClick="javascript:bbcode('[align=center]', '[/align]', 'message_sujet');return(false)" />
						<input type="image" src="./images/right.gif" id="right" name="right" value="aligner � droite" onClick="javascript:bbcode('[align=right]', '[/align]', 'message_sujet');return(false)" />
					<td>
				</tr>
				<tr>
					<td class="creer_gauche">
						Smileys :
					</td>
					<td class="creer_droit">
							<input type="image" src="./images/smiley/heureux.gif" id="heureux" name="heureux" value="heureux" onClick="javascript:smilies(':D', 'message_sujet');return(false)" />
							<input type="image" src="./images/smiley/trema.gif" id="trema" name="trema" value="trema" onClick="javascript:smilies('^^', 'message_sujet');return(false)" />
							<input type="image" src="./images/smiley/sourire.gif" id="sourire" name="sourire" value="sourire" onClick="javascript:smilies(':)', 'message_sujet');return(false)" />
							<input type="image" src="./images/smiley/Oo.gif" id="Oo" name="Oo" value="Oo" onClick="javascript:smilies('Oo', 'message_sujet');return(false)" />
							<input type="image" src="./images/smiley/tkt.gif" id="tkt" name="tkt" value="tkt" onClick="javascript:smilies(';)', 'message_sujet');return(false)" />
							<input type="image" src="./images/smiley/heu.gif" id="heu" name="heu" value="heu" onClick="javascript:smilies(':heu:', 'message_sujet');return(false)" />
							<input type="image" src="./images/smiley/frime.gif" id="frime" name="frime" value="frime" onClick="javascript:smilies(':frime:', 'message_sujet');return(false)" />
							<input type="image" src="./images/smiley/colere.gif" id="colere" name="colere" value="colere" onClick="javascript:smilies(':@', 'message_sujet');return(false)" />
							<input type="image" src="./images/smiley/choc.gif" id="choc" name="choc" value="choc" onClick="javascript:smilies(':O', 'message_sujet');return(false)" />
							<input type="image" src="./images/smiley/triste.gif" id="triste" name="triste" value="triste" onClick="javascript:smilies(':(', 'message_sujet');return(false)" />
							<input type="image" src="./images/smiley/interdit.gif" id="interdit" name="interdit" value="interdit" onClick="javascript:smilies(':interdit:', 'message_sujet');return(false)" />
							<input type="image" src="./images/smiley/reglement.gif" id="reglement" name="reglement" value="reglement" onClick="javascript:smilies(':reglement:', 'message_sujet');return(false)" />
							<input type="image" src="./images/smiley/help.gif" id="help" name="help" value="help" onClick="javascript:smilies(':help:', 'message_sujet');return(false)" />
					<td>
				</tr>
				<tr>
					<td class="creer_gauche" >
						<label for="message_sujet">Message :</label>
					</td>
					<td class="creer_droit" >
						<textarea cols="50" rows="15" name="message_sujet" id="message_sujet"><?php echo $donnees['message']; ?></textarea>
					</td>
				</tr>
			</table>

			<input type="hidden" name="id_message_modif" value='<?php echo $_POST['id_message']; ?>'>
			<input type="hidden" name="sujet" value='<?php echo $donnees['sujet']; ?>'>
			<INPUT type="submit" value="Modifier le message">
		</form>
	</div>

<?php
include('includes/corps_bas.php');
}
}else{
	echo'Vous n\'�tes pas autoris� � �tre ici. <a href="news>"Retour aux news</a>';
	header('location:index.php');
}

?>

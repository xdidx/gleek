<?php
include_once('includes/fonction.php');

$reponse = $bdd->query('SELECT * FROM users WHERE pseudo = \''.$_SESSION['pseudo'].'\' ') or die(mysql_error());
$donnees = $reponse->fetch();
	
$race = explode(',', $donnees['race']);

if(isset($_POST['cheveux']) AND isset($_POST['pieds']) AND isset($_POST['torse']) AND isset($_POST['jambes'])){
	
	$_POST['visage'] = "1";
	$_POST['couleur_visage'] = "0.253.0";
	
	if($_POST['couleur_cheveux']=="0.255.0"){
		$_POST['couleur_cheveux'] = "0.253.0";
	}elseif($_POST['couleur_torse']=="0.255.0"){
		$_POST['couleur_torse'] = "0.253.0";
	}elseif($_POST['couleur_jambes']=="0.255.0"){
		$_POST['couleur_jambes'] = "0.253.0";
	}elseif($_POST['couleur_pieds']=="0.255.0"){
		$_POST['couleur_pieds'] = "0.253.0";
	}
	$new_race = $race[0].','.$_POST['cheveux'].'.'.$_POST['couleur_cheveux'].','.$_POST['visage'].'.'.$_POST['couleur_visage'].','.$_POST['torse'].'.'.$_POST['couleur_torse'].','.$_POST['jambes'].'.'.$_POST['couleur_jambes'].','.$_POST['pieds'].'.'.$_POST['couleur_pieds'];
	
	$req = $bdd->prepare('UPDATE users SET race = ? WHERE pseudo = ?');
	$req->execute(array($new_race, $_SESSION['pseudo']));
	
	header('location:jouer.php?modif_perso');

}elseif(isset($_POST['race']) OR (!isset($race[1]) AND $donnees['niveau'] >0 ) AND $race[0] != ""){
	if(!isset($_POST['race'])){
		$_POST['race'] = $race[0];
	}
	
	$titre="Votre personnage";
	include_once('includes/corps_haut.php');

	$req = $bdd->prepare('UPDATE users SET race = ? WHERE pseudo = ?');
	$req->execute(array($_POST['race'], $_SESSION['pseudo']));
	echo '
	<img src="curseur1.png" id="curseur_barre_cheveux" style="position:absolute;top:0px;left:0px;" alt="curseur_barre"/>
	<!--<img src="curseur1.png" id="curseur_barre_visage" style="position:absolute;top:0px;left:0px;" alt="curseur_barre"/>-->
	<img src="curseur1.png" id="curseur_barre_torse" style="position:absolute;top:0px;left:0px;" alt="curseur_barre"/>
	<img src="curseur1.png" id="curseur_barre_jambes" style="position:absolute;top:0px;left:0px;" alt="curseur_barre"/>
	<img src="curseur1.png" id="curseur_barre_pieds" style="position:absolute;top:0px;left:0px;" alt="curseur_barre"/>
	
	<fieldset>
		<legend>
			<img src="images/personnage.gif" alt="Votre personnage :" />
		</legend>
		
		<table style="width:100%;">
			<tr>
				<td style="text-align:center;" colspan=2>
					Vous avez rejoint le clan des <span class="gras">'.$_POST['race'].'</span>. Veuillez maintenant choisir l\'apparence de votre personnage :<br/><br/>
				</td>
			</tr>
			<tr >
				<td valign="top">
					
					<table>
						<tr style="height:90px;">
							<td style="width:180px;">
								<form action="profil.php" method="post">
									Cheveux : 
									<img src="images/fleche_gauche.png" alt="fleche gauche" onclick="changer_type(\'cheveux\', \'moins\')" style="margin-left:30px;vertical-align: middle;" />
										<input type="hidden" name="cheveux" id="cheveux" value="1"/>							
										<div id="type_cheveux" style="display:inline;">Type 1</div>
									<img src="images/fleche_droite.png" alt="fleche droite" onclick="changer_type(\'cheveux\', \'plus\')" style="vertical-align: middle;margin-right:10px;"/>
										
							</td>
							<td style="text-align:center;">
							
								<table style="padding:0;border-collapse:collapse;margin:auto;width:100%;">
									<tr>
										<td style="padding:0;border:1px solid black;">
											<div id="barre_cheveux" class="barre" style="text-align:center;width:300px;height:20px;background-image:url(\'../degrade.jpg\');" onmousedown="clic=true;" onmousemove="calcul(\'cheveux\', event)" onclick="clic=true;calcul(\'cheveux\', event);clic=false"></div>
										</td>
										<td rowspan="2" style="padding:0;border:1px solid black;">
											<div id="couleur_cheveux" style="text-align:center;background-color:black;height:41px;width:40px;"></div> 
										</td>
									</tr>
									<tr>
										<td style="padding:0;border:1px solid black;">
											<div id="degrade_noir_cheveux" style="text-align:center;height:20px;width:300px;cursor:crosshair;background-color:black;background-image:url(\'degrade_noir.png\');" onclick="clic=true;calcul_noir(\'cheveux\', event)"></div> 
										</td>
									</tr>
								</table>
								<input type="hidden" name="couleur_cheveux" id="couleur_input_cheveux" value="0.0.0">
								
							</td>

						</tr>
						<!--<tr>
							<td >
									Visage : 
							</td>
							<td style="text-align:center;">
									<select onchange="calcul(\'visage\')" name="visage" id="visage" style="margin-left:15px;">
												<option value="1">
													Type A
												</option>
												<option value="2">
													Type B
												</option>
												<option value="3">
													Type C
												</option>
												<option value="4">
													Type D
												</option>
											</select>
								<div id="barre_visage" class="barre" style="text-align:center;width:300px;height:20px;background-image:url(\'../degrade.jpg\');" onclick="clic=true;calcul(\'visage\', event);clic=false;" onmousedown="clic=true;" onmousemove="calcul(\'visage\', event)" ></div> 
								<div id="couleur_visage" style="text-align:center;background-color:black;height:20px;width:300px;"></div> 
								<div id="degrade_noir_visage" style="text-align:center;height:20px;width:300px;cursor:crosshair;background-color:black;background-image:url(\'degrade_noir.png\');" onclick="clic=true;calcul_noir(\'visage\', event);"></div> 
								<input type="hidden" name="couleur_visage" id="couleur_input_visage" value="0.0.0">
							</td>
						</tr>-->
						<tr style="height:90px;">
							<td style="width:180px;">
									Torse : 
									<img src="images/fleche_gauche.png" alt="fleche gauche" onclick="changer_type(\'torse\', \'moins\')" style="margin-left:49px;vertical-align: middle;" />
										<input type="hidden" name="torse" id="torse" value="1"/>							
										<div id="type_torse" style="display:inline;">Type 1</div>
									<img src="images/fleche_droite.png" alt="fleche droite" onclick="changer_type(\'torse\', \'plus\')" style="vertical-align: middle;margin-right:10px;"/>
										
							</td>
							<td style="text-align:center;">
							
								<table style="padding:0;border-collapse:collapse;margin:auto;width:100%;">
									<tr>
										<td style="padding:0;border:1px solid black;">
											<div id="barre_torse" class="barre" style="text-align:center;width:300px;height:20px;background-image:url(\'../degrade.jpg\');" onclick="clic=true;calcul(\'torse\', event);clic=false;" onmousedown="clic=true;" onmousemove="calcul(\'torse\', event)" ></div> 
										</td>
										<td rowspan="2" style="padding:0;border:1px solid black;">
											<div id="couleur_torse" style="text-align:center;background-color:black;height:41px;width:40px;"></div> 
										</td>
									</tr>
									<tr>
										<td style="padding:0;border:1px solid black;">
											<div id="degrade_noir_torse" style="text-align:center;height:20px;width:300px;cursor:crosshair;background-color:black;background-image:url(\'degrade_noir.png\');"   onclick="clic=true;calcul_noir(\'torse\', event);"></div> 
										</td>
									</tr>
								</table>
								<input type="hidden" name="couleur_torse" id="couleur_input_torse" value="0.0.0">
								
							</td>

						</tr>
						<tr style="height:90px;">
							<td style="width:180px;">
									Jambes : 
									<img src="images/fleche_gauche.png" alt="fleche gauche" onclick="changer_type(\'jambes\', \'moins\')" style="margin-left:37px;vertical-align: middle;" />
										<input type="hidden" name="jambes" id="jambes" value="1"/>							
										<div id="type_jambes" style="display:inline;">Type 1</div>
									<img src="images/fleche_droite.png" alt="fleche droite" onclick="changer_type(\'jambes\', \'plus\')" style="vertical-align: middle;margin-right:10px;"/>
										
							</td>
							<td style="text-align:center;">
							
								<table style="padding:0;border-collapse:collapse;margin:auto;width:100%;">
									<tr>
										<td style="padding:0;border:1px solid black;">
											<div id="barre_jambes" class="barre" style="text-align:center;width:300px;height:20px;background-image:url(\'../degrade.jpg\');" onclick="clic=true;calcul(\'jambes\', event);clic=false;" onmousedown="clic=true;" onmousemove="calcul(\'jambes\', event)"  ></div> 
										</td>
										<td rowspan="2" style="padding:0;border:1px solid black;">
											<div id="couleur_jambes" style="text-align:center;background-color:black;height:41px;width:40px;"></div> 
										</td>
									</tr>
									<tr>
										<td style="padding:0;border:1px solid black;">
											<div id="degrade_noir_jambes" style="text-align:center;height:20px;width:300px;cursor:crosshair;background-color:black;background-image:url(\'degrade_noir.png\');" onclick="clic=true;calcul_noir(\'jambes\', event)"></div> 
										</td>
									</tr>
								</table>
								<input type="hidden" name="couleur_jambes" id="couleur_input_jambes" value="0.0.0">
								
							</td>

						</tr>
						<tr style="height:90px;">
							<td style="width:180px;">
								<form action="profil.php" method="post">
									Pieds : 
									<img src="images/fleche_gauche.png" alt="fleche gauche" onclick="changer_type(\'pieds\', \'moins\')" style="margin-left:50px;vertical-align: middle;" />
										<input type="hidden" name="pieds" id="pieds" value="1"/>							
										<div id="type_pieds" style="display:inline;">Type 1</div>
									<img src="images/fleche_droite.png" alt="fleche droite" onclick="changer_type(\'pieds\', \'plus\')" style="vertical-align: middle;margin-right:10px;"/>
										
							</td>
							<td style="text-align:center;">
							
								<table style="padding:0;border-collapse:collapse;margin:auto;width:100%;">
									<tr>
										<td style="padding:0;border:1px solid black;">
											<div id="barre_pieds" class="barre" style="text-align:center;width:300px;height:20px;background-image:url(\'../degrade.jpg\');" onclick="clic=true;calcul(\'pieds\', event);clic=false;" onmousedown="clic=true;" onmousemove="calcul(\'pieds\', event)"  ></div> 
										</td>
										<td rowspan="2" style="padding:0;border:1px solid black;">
											<div id="couleur_pieds" style="text-align:center;background-color:black;height:41px;width:40px;"></div> 
										</td>
									</tr>
									<tr>
										<td style="padding:0;border:1px solid black;">
											<div id="degrade_noir_pieds" style="text-align:center;height:20px;width:300px;cursor:crosshair;background-color:black;background-image:url(\'degrade_noir.png\');" onclick="clic=true;calcul_noir(\'pieds\', event)"></div> 
										</td>
									</tr>
								</table>
								<input type="hidden" name="couleur_pieds" id="couleur_input_pieds" value="0.0.0">	
							</td>
						</tr>
					</table>								
					</td>
					<td style="text-align:center;">
						<fieldset>
							<legend>
								<img src="images/apercu.gif" alt="Aper�u" />
							</legend>
							<table style="margin:auto;width:100%;">
								<tr>
									<td colspan=2>
										<div id="image_apercu_face"><img height="96" width="64" src="combiner.php?pos=face&apercu='.$_POST['race'].',1.0.0.0,undefined.undefined,1.0.0.0,1.0.0.0,1.0.0.0"/></div>
									</td>
								</tr>
								<tr>
									<td>
									<div id="image_apercu_gauche"><img height="96" width="64" src="combiner.php?pos=gauche&apercu='.$_POST['race'].',1.0.0.0,undefined.undefined,1.0.0.0,1.0.0.0,1.0.0.0"/></div>
									</td>
									<td>
										<div id="image_apercu_droite"><img height="96" width="64" src="combiner.php?pos=droite&apercu='.$_POST['race'].',1.0.0.0,undefined.undefined,1.0.0.0,1.0.0.0,1.0.0.0"/></div>
									</td>
								</tr>	
								<tr>
									<td colspan=2>
										<div id="image_apercu_dos"><img height="96" width="64" src="combiner.php?pos=dos&apercu='.$_POST['race'].',1.0.0.0,undefined.undefined,1.0.0.0,1.0.0.0,1.0.0.0"/></div>
								</tr>
							</table>
						</fieldset>
					</td>
				</tr>	
				<tr>
					<td style="text-align:center;" colspan=2>
						<input type="image" src="images/valider.png" style=margin-top:10px;">
					</td>
				</tr>	
				</form>
		</table>
	</fieldset>';
	

	
	include_once('includes/corps_bas.php');
	
}elseif($race[0] == "" AND $donnees['niveau'] >0 ){
header('location:jouer.php');

}elseif(isset($_POST['password_actuel']) AND isset($_POST['email_profil'])){

	$password = $_POST['password_new'];
	$email = $_POST['email_profil'];
	$signature = $_POST['signature_profil'];
	$password_actuel = md5($_POST['password_actuel']);
	$i="succes";
	
	$reponse= $bdd->query("SELECT * FROM users WHERE pseudo='".$_SESSION['pseudo']."'") or die(mysql_error());
	$donnees = $reponse->fetch();
	if($password == ""){
		$password = $donnees['password'];
	}
	elseif($password_actuel != $donnees['password']){
		$password = $donnees['password'];
		$i="erreur"; 
	}
	else{
		$password = md5($password);
	}
	if($_FILES['avatar']['name'] != ""){
		$extension = strtolower(substr(strrchr($_FILES['avatar']['name'], '.')  ,1));
		if($extension == "gif" OR $extension == "jpg" OR $extension == "png" OR $extension == "jpeg" OR $extension == "bmp" OR $extension == "ico"){
			if($_FILES['avatar']['size'] <70000){
				move_image($_FILES['avatar'], 'avatars/'.$_SESSION['pseudo']);
				$req = $bdd->prepare('UPDATE users SET password = ?, signature = ?, email = ?, avatar = ? WHERE pseudo = ?');
				$req->execute(array($password, $signature, $email, $extension, $_SESSION['pseudo']));
				header('location:profil.php?'.$i);
			}else{
				$req = $bdd->prepare('UPDATE users SET password = ?, signature = ?, email = ? WHERE pseudo = ?');
				$req->execute(array($password, $signature, $email, $_SESSION['pseudo']));
				header('location:profil.php?'.$i.'=size');
			}
		}else{
			header('location:profil.php?'.$i.'=ext');
		}
	}else{
		$req = $bdd->prepare('UPDATE users SET password = ?, signature = ?, email = ? WHERE pseudo = ?');
		$req->execute(array($password, $signature, $email, $_SESSION['pseudo']));
		header('location:profil.php?'.$i);
	}

	
}elseif(isset($_SESSION['login']) AND $_SESSION['login'] == 0 AND !isset($_POST['code'])){

	$titre="Code de confirmation";
	include_once('includes/corps_haut.php');
	
	if(isset($_GET['errone'])){
		echo 'Votre code est erron�';
	}
	
	echo'<div style="text-align:center;">
			<FORM method=post action="profil.php">
				Veuillez saisir le code re�u par email :
				<br/>
				<input type="text" name="code" size="30">
				<br/>			
				<INPUT type="image" style="margin:5px;" src="images/valider.png" value="Valider">
			</FORM>
		</div>';
	include_once('includes/corps_bas.php');
		
}elseif(isset($_SESSION['login']) AND $_SESSION['login'] > 0){

	$titre="Votre profil";
	include_once('includes/corps_haut.php');
		
	$reponse= $bdd->query("SELECT * FROM users WHERE pseudo='".$_SESSION['pseudo']."'") or die(mysql_error());
	$donnees = $reponse->fetch();
	
	echo '
	<form method="post" enctype="multipart/form-data"  action="profil.php">
		<table style="text-align:left;margin:auto;margin-top:15px;">
			<tr>
				<td colspan=2>';
						if(isset($_GET['erreur'])){
							echo '<span style="color:red;">Une erreur est survenue dans votre mot de passe.<br/></span>';
						}elseif(isset($_GET['succes']) AND $_GET['succes'] == "size"){
							echo '<span style="color:red;">Votre profil a bien �t� modifier mais votre avatar est trop volumineux.<br/>Veuillez modifier sa taille et reesayer la manipulation. Merci<br/><br/></span>';
						}elseif(isset($_GET['succes']) AND $_GET['succes'] == "ext"){
							echo '<span style="color:red;">Votre profil a bien �t� modifier mais votre avatar a une extention non pris en charge.<br/>Veuillez le modifier et reesayer la manipulation. Merci<br/><br/></span>';
						}elseif(isset($_GET['succes'])){
							echo '<span style="color:green;">Votre profil a �t� modifi� avec succ�s.<br/></span>';
						}
						
				echo 	'Si vous ne souhaitez pas changer de mot de passe, laissez les champs vides.<br/><br/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="password_actuel">Mot de passe actuel :</label>
				</td>
				<td>
					<input type="password" id="password_actuel" name="password_actuel" style="width:200px;">
				</td>
			</tr>
			<tr>
				<td>
					<label for="password_new">Nouveau mot de passe :</label>
				</td>
				<td>
					<input type="password" onkeyup="verif_password_new();" id="password_inscription"   name="password_new" style="width:200px;">
				</td>
			</tr>
			<tr>
				<td>
					<label for="confirm_inscription">Confirmation :</label>
				</td>
				<td>
					<input type="password" onkeyup="verif_confirm();" id="confirm_inscription" name="confirmation"   value="" style="width:200px;">
				</td>
			</tr>
			<tr>
				<td>
					<label for="email_inscription">Email:</label>
				</td>
				<td>
					<input type="text" id="email_inscription" onkeyup="verif_email_m();" name="email_profil" value="'.$donnees['email'].'" style="width:200px;"	>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<label for="signature">Signature :</label>
				</td>
				<td>
					<textarea name="signature_profil" id="signature" rows="7" cols="40">'.$donnees['signature'].'</textarea>
				</td>
			</tr>
			<tr>
				<td>
					<label for="avatar">Avatar</label>
				</td>
				<td>
					<INPUT type="file" id="avatar" name="avatar" >
				</td>
			</tr>
			<tr>
				<td colspan=2 style="text-align:center;margin-top:5px;">
					<INPUT type="image" onclick="modification();return false;" id="submit_inscription" style="margin:5px;"  src="images/valider.png" value="Valider">
				</td>
			</tr>
		</table>
	</form>
	';
	
	include_once('includes/corps_bas.php');
	
}elseif(isset($_POST['code'])){

	$reponse = $bdd->query('SELECT * FROM users WHERE pseudo = \''.$_SESSION['pseudo'].'\' ') or die(mysql_error());
	$donnees = $reponse->fetch();
	if(md5($donnees['pseudo'].'_'.$donnees['email']) == $_POST['code']){
			
		$req = $bdd->prepare('UPDATE users SET niveau = ? WHERE pseudo = ?');
		$req->execute(array("1", $_SESSION['pseudo']));
		$_SESSION['login'] = 1;
		header('location:jouer.php?valider');
	}else{
		header('location:profil.php?errone');
	}
}else{

	header('location:acces.php');
}


?>
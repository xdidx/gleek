var tileset = new Image();
tileset.src = "images/tilesets/standart.png";

var perso = new Image();
//perso.src = "combiner.php?sprite="+file('verif.php?race_complete');
perso.src = "images/races/test.png";


var carte = JSON.parse(file('maps/premiere.json'));

var canvas = document.getElementById('canvas');
var ctx = canvas.getContext('2d');

var etat_anim = 0;
var etat_animation = 0;
var vitesse_anim_1 = 80;
var vitesse_anim_2 = vitesse_anim_1*2;
var vitesse_anim_3 = vitesse_anim_1*3;

window.onload = function() {
	carte_perso("autre");
}
function dessiner_carte(nb_pixel, direction) {
	var pos_x = parseInt(file('verif.php?pos_x'));
	var pos_y = parseInt(file('verif.php?pos_y'));
		
	var a = -1;
	var b = -1;
	for(i = pos_y-8; i < pos_y + 10 ; i++) {
		for(j = pos_x-8; j < pos_x + 10 ; j++) {
			if(i<0 || j<0){

				carte_tileset(17,b,a, nb_pixel, direction);
			}else if(i>=carte.terrain.length || j>=carte.terrain[i].length){
			
				carte_tileset(17,b,a, nb_pixel, direction);
			}else{
				carte_tileset(carte.terrain[i][j],b,a,nb_pixel, direction);
			}
			b++;
		}
		a++;
		b = -1;
	}

}

function carte_tileset(numero, x, y, nb_pixel, direction){

	var image_x=(numero-1)%4*32;
	
	var image_y = Math.ceil(numero / 4);
	image_y = (image_y-1)*32;

	if(direction == "gauche"){
		
		x = 32*(x-1)+nb_pixel;
		y = 32*y;
		
	}else if(direction == "droite"){
	
		x = 32*(x+1)-nb_pixel;
		y = 32*y;
		
	}else if(direction == "haut"){
		
		x = 32*x;
		y = 32*(y-1)+nb_pixel;
	
	}else if(direction == "bas"){
		
		x = 32*x;
		y = 32*(y+1)-nb_pixel;
	
	}else{
	
		x = 32*x;
		y = 32*y;
		
	}

	ctx.drawImage(tileset, image_x, image_y, 32, 32, x, y, 32, 32);

}



 window.requestAnimFrame = (function(){
	
      return  window.requestAnimationFrame      || 
              window.webkitRequestAnimationFrame || 
              window.mozRequestAnimationFrame    || 
              window.oRequestAnimationFrame      || 
              window.msRequestAnimationFrame     || 
              function(/* function */ callback, /* DOMElement */ element){
                window.setTimeout(callback, 1000 / 60);
              };
	})();
	
	
	
var pixels = 8;
var ok = true;
var temps_actuel;
var temps_depart;
var date;
var image_1 = true;
var image_2 = true;
var image_3 = true;
var dir = 'bas';


function render(){

	etat_anim = 1;
	temps_actuel = (new Date()).getTime();
	date = (temps_actuel-temps_depart);
	document.getElementById('temps').innerHTML = date;

	if(date<vitesse_anim_1 && image_1){
		pixels = 8;
		ok = true;
		image_1 = false;
	}else if(date>vitesse_anim_1 && image_2){
		pixels = 16;
		ok = true;
		image_2 = false;
	}else if(date>vitesse_anim_2 && image_3){
		pixels = 24;
		ok = true;
		image_3 = false;
	}else if(date>vitesse_anim_3){
		pixels = 32;
		ok = true;
	}else{
		ok = false;
	}
	if(ok){
		
		if(pixels < 32){
			dessiner_carte(pixels, dir);
			if(dir == "bas"){
				ctx.drawImage(perso, (pixels*4), 0, 32, 48, 223, 206, 32, 48);
			}else if(dir == "gauche"){
				ctx.drawImage(perso, (pixels*4), 48, 32, 48, 223, 206, 32, 48);
			}else if(dir == "droite"){
				ctx.drawImage(perso, (pixels*4), 96, 32, 48, 223, 206, 32, 48);
			}else if(dir == "haut"){
				ctx.drawImage(perso, (pixels*4), 144, 32, 48, 223, 206, 32, 48);
			}else{
				alert("bug");
			}
			requestAnimFrame(render);
		}else{

			dessiner_carte(0, 'autre');
			if(dir == "bas"){
				ctx.drawImage(perso, 0, 0, 32, 48, 223, 206, 32, 48);
			}else if(dir == "gauche"){
				ctx.drawImage(perso, 0, 48, 32, 48, 223, 206, 32, 48);
			}else if(dir == "droite"){
				ctx.drawImage(perso, 0, 96, 32, 48, 223, 206, 32, 48);
			}else if(dir == "haut"){
				ctx.drawImage(perso, 0, 144, 32, 48, 223, 206, 32, 48);
			}else{
				alert("bug");
			}
			pixels = 8;
			etat_anim = 0;
			image_1 = true;
			image_2 = true;
			image_3 = true;
			ok = false;	
		}
	}else{
		requestAnimFrame(render);
	}
	
}

function carte_perso(direction, autorise){
	if(etat_anim == 0){
		dessiner_carte(0, direction);
		ctx.drawImage(perso, 0, 0, 32, 48, 223, 206, 32, 48);
	}
}

/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////Clavier///////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
document.addEventListener('keydown', function(e) {
	if(etat_anim == 0){
		if(e.which == 37 || e.which == 113 || e.which == 97 || e.which == 81 || e.which == 65){
			dir = 'gauche';
		}else if(e.which == 38 || e.which == 122 || e.which == 119 || e.which == 90 || e.which == 87){
			dir = 'haut';
		}else if(e.which == 39 || e.which == 100 || e.which == 68){
			dir = 'droite';
		}else if(e.which == 40 || e.which == 115 || e.which == 83){
			dir = 'bas';
		}
		
		var avant = file('verif.php?pos='+dir);
		var apres = file('verif.php?pos='+dir+'&length='+carte.terrain[0].length);
		if(avant != apres){
			temps_depart = (new Date()).getTime();
			requestAnimFrame(render(dir));
		}
	}
}, false);


/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////SOURIS/////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
function bouger_souris(event){
	if(etat_animation == 0 && etat_anim == 0){
		var pos_canvas_top = (event.clientY)-(getPosition_top('canvas')-2*document.body.scrollTop);
		var pos_canvas_left = event.clientX-getPosition_left('canvas');
		var nb_cases_hauteur = 0;
		var nb_cases_largeur = 0;
		etat_animation = 1;
		//CALCUL
		if(pos_canvas_top > 0 && pos_canvas_top < 224){
			nb_cases_hauteur = (224-pos_canvas_top)/32;
		}else if(pos_canvas_top > 224 && pos_canvas_top < 256){
			nb_cases_hauteur = 0;
		}else if(pos_canvas_top > 256 && pos_canvas_top < 480){
			 nb_cases_hauteur = (256-pos_canvas_top)/32;
		}
		if(pos_canvas_left > 0 && pos_canvas_left < 224){
			 nb_cases_largeur = (224-pos_canvas_left)/32;
		}else if(pos_canvas_left > 224 && pos_canvas_left < 256){
			 nb_cases_largeur = 0;
		}else if(pos_canvas_left> 256 && pos_canvas_left < 480){
			 nb_cases_largeur = (256-pos_canvas_left)/32;
		}
		
		if(nb_cases_hauteur == 0){
			if(nb_cases_largeur == 0){

			}else if(nb_cases_largeur > 0){
				
				for(i = 0; i < nb_cases_largeur ; i++){//gauche
					etat_animation= 1;
					setTimeout(function(){
						var avant = file('verif.php?pos=gauche');
						var apres = file('verif.php?pos=gauche&length='+carte.terrain[0].length);
						if(avant != apres){
							carte_perso("gauche", true);
						}else{
							carte_perso("gauche", false);
						}
					}
					,i*800);
				}
			
			}else if(nb_cases_largeur < 0){
				nb_cases_largeur = Math.abs(nb_cases_largeur);//droite

				for(i = 0; i < nb_cases_largeur ; i++){
					
					setTimeout(function(){
						var avant = file('verif.php?pos=droite');
						var apres = file('verif.php?pos=droite&length='+carte.terrain[0].length);
						if(avant != apres){
							carte_perso("droite", true);
						}else{
							carte_perso("droite", false);
						}
					}
					,i*800);
				}
			}
			
		}else if(nb_cases_hauteur > 0){
			if(nb_cases_largeur == 0){
			
				for(i = 0; i < nb_cases_hauteur ; i++){//HAUT
					
					setTimeout(function(){
						var avant = file('verif.php?pos=haut');
						var apres = file('verif.php?pos=haut&length='+carte.terrain.length);
						if(avant != apres){
							carte_perso("haut", true);
						}else{
							carte_perso("haut", false);
						}
					}
					,i*800);
				}
				
			}else if(nb_cases_largeur > 0){//DIAGONALE HAUT GAUCHE
			
				for(i = 0; i < nb_cases_largeur ; i++){
					
					setTimeout(function(){
						var avant = file('verif.php?pos=gauche');
						var apres = file('verif.php?pos=gauche&length='+carte.terrain[0].length);
						if(avant != apres){
							carte_perso("gauche", true);
						}else{
							carte_perso("gauche", false);
						}
					}	
					,i*800);
				}
				setTimeout(function(){
					for(i = 0; i < nb_cases_hauteur ; i++){
						setTimeout(function(){
							
							var avant = file('verif.php?pos=haut');
							var apres = file('verif.php?pos=haut&length='+carte.terrain.length);
							if(avant != apres){
								carte_perso("haut", true);
							}else{
								carte_perso("haut", false);
							}
						}
						,i*800);
					}
				},nb_cases_largeur*800);
			
			}else if(nb_cases_largeur < 0){//DIAGONALE HAUT DROITE
				nb_cases_largeur = Math.abs(nb_cases_largeur);
				
				for(i = 0; i < nb_cases_largeur ; i++){
					setTimeout(function(){
						
						var avant = file('verif.php?pos=droite');
						var apres = file('verif.php?pos=droite&length='+carte.terrain[0].length);
						if(avant != apres){
							carte_perso("droite", true);
						}else{
							carte_perso("droite", false);
						}
					}	
					,i*800);
				}
				setTimeout(function(){
					for(i = 0; i < nb_cases_hauteur ; i++){
						
						setTimeout(function(){
							
							var avant = file('verif.php?pos=haut');
							var apres = file('verif.php?pos=haut&length='+carte.terrain.length);
							if(avant != apres){
								carte_perso("haut", true);
							}else{
								carte_perso("haut", false);
							}
						}
						,i*800);
					}
				},nb_cases_largeur*800);

			}			
			
		}else if(nb_cases_hauteur < 0){
			nb_cases_hauteur = Math.abs(nb_cases_hauteur);
			if(nb_cases_largeur == 0){//BAS
				
				for(i = 0; i < nb_cases_hauteur ; i++){
					
					
					setTimeout(function(){						
						var avant = file('verif.php?pos=bas');
						var apres = file('verif.php?pos=bas&length='+carte.terrain.length);
						if(avant != apres){
							carte_perso("bas", true);
						}else{
							carte_perso("bas", false);
						}
					}
					,i*800);
				}
				
			}else if(nb_cases_largeur > 0){//DIAGONALE BAS GAUCHE
			
				for(i = 0; i < nb_cases_largeur ; i++){
					
					setTimeout(function(){
						var avant = file('verif.php?pos=gauche');
						var apres = file('verif.php?pos=gauche&length='+carte.terrain[0].length);
						if(avant != apres){
							carte_perso("gauche", true);
						}else{
							carte_perso("gauche", false);
						}
					}	
					,i*800);
				}
				setTimeout(function(){
					for(i = 0; i < nb_cases_hauteur ; i++){
						setTimeout(function(){
							
							var avant = file('verif.php?pos=bas');
							var apres = file('verif.php?pos=bas&length='+carte.terrain.length);
							if(avant != apres){
								carte_perso("bas", true);
							}else{
								carte_perso("bas", false);
							}
						}
						,i*800);
					}
				},nb_cases_largeur*800);
			
			}else if(nb_cases_largeur < 0){//DIAGONALE BAS DROITE
				nb_cases_largeur = Math.abs(nb_cases_largeur);
				
				for(i = 0; i < nb_cases_largeur ; i++){
					setTimeout(function(){
						
						var avant = file('verif.php?pos=droite');
						var apres = file('verif.php?pos=droite&length='+carte.terrain[0].length);
						if(avant != apres){
							carte_perso("droite", true);
						}else{
							carte_perso("droite", false);
						}
					}	
					,i*800);
				}
				setTimeout(function(){
					for(i = 0; i < nb_cases_hauteur ; i++){
						setTimeout(function(){
							
							var avant = file('verif.php?pos=bas');
							var apres = file('verif.php?pos=bas&length='+carte.terrain.length);
							if(avant != apres){
								carte_perso("bas", true);
							}else{
								carte_perso("bas", false);
							}
						}
						,i*800);
					}
				},nb_cases_largeur*800);

			}	
			
		}
		
		setTimeout("etat_animation= 0;", nb_cases_hauteur*800+nb_cases_largeur*800); 
		
	}
}
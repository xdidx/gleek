
function file(fichier)
{
	if(window.XMLHttpRequest) // FIREFOX
	  xhr_object = new XMLHttpRequest();
	else if(window.ActiveXObject) // IE
	  xhr_object = new ActiveXObject("Microsoft.XMLHTTP");
	else
	  return(false);
	xhr_object.open("GET", fichier, false);
	xhr_object.send(null);
	if(xhr_object.readyState == 4) return(xhr_object.responseText);
	else return(false);
	
}

function verif_pseudo(){
	var pseudo = document.getElementById('pseudo_inscription'); 
	var verif = /^[a-zA-Z0-9_-]{4,}$/;
	if (verif.exec(pseudo.value) == null){
		pseudo.style.border = "3px solid red";
		return false;
	}else if(file('verif.php?pseudo='+pseudo.value) != 0){
		pseudo.style.border = "3px solid red";
		return false;
	}else{
		pseudo.style.border = "3px solid green";
		return true;
	}
}
	
function verif_email(){
	var email = document.getElementById('email_inscription'); 
	var verif = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]{2,}[.][a-zA-Z]{2,3}$/;
	if (verif.exec(email.value) == null){
		email.style.border = "3px solid red";
		return false;
	}else if(file('verif.php?email='+email.value) != 0){
		email.style.border = "3px solid red";
		return false;
	}else{
		email.style.border = "3px solid green";
		return true;
	}
}

function verif_signature(){
	var pseudo = document.getElementById('signature'); 
	var verif = /^[a-zA-Z0-9_-]$/;
	if (pseudo.value.length > 200){
		pseudo.style.border = "3px solid red";
		return false;
	}else if(file('verif.php?signature='+pseudo.value) != pseudo.value){
		pseudo.style.border = "3px solid red";
		return false;
	}else{
		pseudo.style.border = "3px solid green";
		return true;
	}
}

function verif_password(){
	var password = document.getElementById('password_inscription'); 
	var verif = /^[a-zA-Z0-9_-]{4,}$/;
	verif_confirm();
	if (verif.exec(password.value) == null){
		password.style.border = "3px solid red";
		return false;
	}else{
		password.style.border = "3px solid green";
		return true;
	}
}

function verif_password_new(){
	var password = document.getElementById('password_inscription'); 
	var verif = /^[a-zA-Z0-9_-]{4,}$/;
	verif_confirm();
	var password2 = document.getElementById('password_actuel').value; 
	if(password2 == ""){
		return true;		
	}else if (verif.exec(password.value) == null){
		password.style.border = "3px solid red";
		return false;
	}else{
		password.style.border = "3px solid green";
		return true;
	}
}

function verif_password_profil(){
	var password = document.getElementById('password_actuel'); 
	var verif = /^[a-zA-Z0-9_-]{4,}$/;
	var password2 = document.getElementById('password_actuel').value; 
	if(password2 == ""){
		return true;
	}else if (verif.exec(password.value) == null){
		password.style.border = "3px solid red";
		return false;
	}else{
		password.style.border = "3px solid green";
		return true;
	}
}

function verif_confirm(){
	var password = document.getElementById('password_inscription'); 
	var confirm = document.getElementById('confirm_inscription'); 
	if (password.value != confirm.value){
		confirm.style.border = "3px solid red";
		return false;
	}else{
		confirm.style.border = "3px solid green";
		return true;
	}
}

function inscription(){	
	var submit = document.getElementById('submit_inscription');

	if(verif_pseudo() && verif_password() && verif_confirm() && verif_email() && verif_signature()){
		submit.submit();
	}else{
		alert('Des erreurs sont survenues durant votre inscription, veuillez les corriger.');
	}
}

function mdp_perdu(){	

	var email = document.getElementById('email_mdp'); 
	var verif = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]{2,}[.][a-zA-Z]{2,3}$/;
	if(file('verif.php?email='+email.value) == 0){
		alert('L\'adresse email n\'est pas valide ou n\'est pas r�pertori� sur notre site.');
		return false;
	}else{
		submit();
	}

}

function modification(){	
	var submit = document.getElementById('submit_inscription');
	
	if(verif_password_new() && verif_password_profil() && verif_email_m()){
		submit.submit();
	}else{
		alert('Veuillez corriger les erreurs indiqu�es');
	}

}

function verif_email_m(){
	var email = document.getElementById('email_inscription'); 
	var verif = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]{2,}[.][a-zA-Z]{2,3}$/;
	if (verif.exec(email.value) == null){
		email.style.border = "3px solid red";
		return false;
	}else if(file('verif.php?email='+email.value) != 0 && email.value != mail){
		email.style.border = "3px solid red";
		return false;
	}else{
		email.style.border = "3px solid green";
		return true;
	}
}

function bbcode(bbdebut, bbfin, id)
{
	var input = document.getElementById(id);
	input.focus();
	if(typeof document.selection != 'undefined')
	{
		var range = document.selection.createRange();
		var insText = range.text;
		range.text = bbdebut + insText + bbfin;
		range = document.selection.createRange();
		
		if (insText.length == 0)
		{
			range.move('character', -bbfin.length);
		}
		else
		{
			range.moveStart('character', bbdebut.length + insText.length + bbfin.length);
		}
		
		range.select();
	}
	else if(typeof input.selectionStart != 'undefined')
	{
		var start = input.selectionStart;
		var end = input.selectionEnd;
		var insText = input.value.substring(start, end);
		input.value = input.value.substr(0, start) + bbdebut + insText + bbfin + input.value.substr(end);
		var pos;
		
		if (insText.length == 0)
		{
			pos = start + bbdebut.length;
		}
		else
		{
			pos = start + bbdebut.length + insText.length + bbfin.length;
		}
		
		input.selectionStart = pos;
		input.selectionEnd = pos;
	}
	 
	else
	{
		var pos;
		var re = new RegExp('^[0-9]{0,3}$');
		
		while(!re.test(pos))
		{
			pos = prompt("insertion (0.." + input.value.length + "):", "0");
		}
		
		if(pos > input.value.length)
		{
			pos = input.value.length;
		}
		
		var insText = prompt("Veuillez taper le texte");
		input.value = input.value.substr(0, pos) + bbdebut + insText + bbfin + input.value.substr(pos);
	}
}

function smilies(img, id)
{
	 document.getElementById(id).value += '' + img + '';
}
function switch_spoiler(element){
        if(element.firstChild.style.display==="block")
        {
				element.firstChild.style.display="none";
        }
        else
        {
                element.firstChild.style.display="block";
        }
        return true;

}
function cocher(id, nb)
{
	document.getElementById(''+id+'').checked=true;
	document.getElementById('race_1').style.backgroundColor = "";
	document.getElementById('race_2').style.backgroundColor = "";
	document.getElementById('race_3').style.backgroundColor = "";
	document.getElementById('race_4').style.backgroundColor = "";
	document.getElementById('race_5').style.backgroundColor = "";
	document.getElementById('race_'+nb+'').style.backgroundColor = "#d2d2d2";
	document.getElementById('description_race').innerHTML = file('verif.php?race_desc='+id);
	$("#image_race").attr('src', 'images/races/'+id+'/'+id+'.gif');
	$("#image_race").attr('alt', id);
}

function couleur(parti, nb)
{
	document.getElementById(parti+'_1').style.borderColor = "";
	document.getElementById(parti+'_2').style.borderColor = "";
	document.getElementById(parti+'_3').style.borderColor = "";
	document.getElementById(parti+'_4').style.borderColor = "";
	//document.getElementById(parti+'_5').style.borderColor = "";
	document.getElementById(parti+'_6').style.borderColor = "";
	document.getElementById(parti+'_'+nb).style.borderColor = "black";
}

function succes(i){
document.getElementById('points_'+i).innerHTML = file('verif.php?points='+i);
}

function succes2(i){
document.getElementById('points_'+i).innerHTML = file('verif.php?succes='+i);
}



function changer_type(partie, sens)
{
	if(sens == "plus"){
		if($('#'+partie).val()<4){
		
			$('#'+partie).val(parseInt($('#'+partie).val())+1);
			
			document.getElementById('type_'+partie).innerHTML = "Type "+$('#'+partie).val();
			
			calcul('autres');
		}
	}else if(sens == "moins"){
		if($('#'+partie).val()>1){
			$('#'+partie).val(parseInt($('#'+partie).val())-1);
			document.getElementById('type_'+partie).innerHTML="Type "+$('#'+partie).val();
			calcul('autres');
		}	
	}
}


function getPosition_left(element)
{
	var left = 0;
	/*On r�cup�re l'�l�ment*/
	var e = document.getElementById(element);
	
	/*Tant que l'on a un �l�ment parent*/
	while (e!=null && e.offsetParent != undefined && e.offsetParent != null)
	{
		/*On ajoute la position de l'�l�ment parent*/
		left += e.offsetLeft + (e.clientLeft != null ? e.clientLeft : 0);
		e = e.offsetParent;
	}
	return left;
}
function getPosition_top(element)
{
	var top = 0;
	/*On r�cup�re l'�l�ment*/
	var e = document.getElementById(element);
	/*Tant que l'on a un �l�ment parent*/
	while (e.offsetParent != undefined && e.offsetParent != null)
	{
		/*On ajoute la position de l'�l�ment parent*/
		top += e.offsetTop + (e.clientTop != null ? e.clientTop : 0);
		e = e.offsetParent;
	}
	top = top+document.body.scrollTop;
	return top;
}

function liste_pseudo(input, liste){
	document.getElementById(liste).style.display="block";
	document.getElementById(liste).style.top = getPosition_top(input)-document.body.scrollTop+20+'px';
	document.getElementById(liste).style.left = getPosition_left(input)-2+'px';
	document.getElementById(liste).innerHTML = file('verif.php?debut_pseudo='+$('#'+input).val()+'&div_liste='+liste+'&input='+input);
}

function verif_new_mp(){
	var i = 0;
	if(!file('verif.php?existe_pseudo='+$('#pour').val())){
		alert('Destinataire non valide.');
		i++;
	}
	
	if(file('verif.php?pseudo_session='+$('#pour').val())){
		alert('Vous ne pouvez pas vous envoyez de messages. D�bile!');
		i++;
	}
	
	if($('#sujet').val() == ""){
		alert('Veuillez renseigner un sujet');
		i++;
	}

	if($('#message_reponse').val() == ""){
		alert('Veuillez renseigner un message');
		i++;
	}
	if(i==0){
		submit_mp.submit();
	}
}
<?php

include('../includes/fonction.php');

if(!isset($_SESSION['login']) OR $_SESSION['login'] < 2)
{
	header('Location: ../inscription.php');
}

$titre = "Gestion des catégories";
include('includes/corps_haut.php');

if(isset($_POST['new_name_cate'])){
	
	$req = $bdd->prepare('INSERT INTO categories(titre) VALUES(?)');
	$req->execute(array($_POST['new_name_cate']));

	echo'<div class="info">Catégorie cr&eacute;e!</div><br/>';

}


if (isset($_POST['titre_modif'])){
	$req = $bdd->prepare('UPDATE categories SET titre = ? WHERE id = ?');
	$req->execute(array($_POST['titre_modif'], $_POST['id_modif']));
	
	$req = $bdd->prepare('UPDATE forums SET categorie = ? WHERE categorie = ?');
	$req->execute(array($_POST['titre_modif'], $_POST['titre_cate']));
	 
	echo'<div class="info">Catégorie modifi&eacute;e!</div><br/>';
}

if (isset($_GET['suppr'])){
	echo'<div class="info">Catégorie supprim&eacute;e!</div><br/>';
}

if (isset($_POST['modif'])){
	if ($_POST['modif'] == "modifier"){
		$reponse_modif = $bdd->prepare('SELECT * FROM categories where id=?') or die(mysql_error());
		$reponse_modif->execute(array($_POST['id_cate']));

		$donnees_modif = $reponse_modif->fetch();
		
		echo '
		<FORM method=post action="cate.php">
			<table>
				<tr>
					<td>
						<label for="titre">Titre</label>
					</td>
					<td>
						<input type="text" name="titre_modif" id="titre" value="'. $donnees_modif['titre'] .'"/>
					</td>
				</tr>
				<tr style="text-align:center;">
					<td colspan="2">
						<INPUT type="submit" value="Valider">
					</td>
				</tr>
			</table>
				
			<input type="hidden" name="titre_cate" value="'. $donnees_modif['titre'].'"/>
			<input type="hidden" name="id_modif" value="'. $_POST['id_cate'] .'"/>
		</FORM>';
		
	}
	elseif ($_POST['modif'] == "supprimer")
	{
		$reponse = $bdd->prepare('DELETE FROM categories WHERE id = ?');
		$reponse->execute(array($_POST['id_cate']));
		header('Location: cate.php?suppr');   
	}
	else
	{
		echo 'Il y a un soucis, veuillez contacter un administrateur.';
	}
}


?>

<table class="classement">
	<tr>
		<td>
			Titre
		</td>
		<td>
			Action
		</td>
	</tr>
	<?php
	$reponse = $bdd->query('SELECT * FROM categories') or die(mysql_error());
	while ($donnees = $reponse->fetch())
	{
		if ($i&1){
			$color = '#d2d2d2';
		}else{
			$color = '#ffffff';
		}
		echo '
		<tr style="background-color:'.$color.';">
			<td>
				'.$donnees['titre'].'
			</td>
			<td>
				<FORM method=post action="cate.php">
					<SELECT name="modif">
						<OPTION VALUE="modifier">Modifier</OPTION>
						<OPTION VALUE="supprimer">Supprimer</OPTION>
					</SELECT>
					<input type="hidden" name="id_cate" value='. $donnees['id'].'/>
					
					<INPUT type="submit" value="Valider">
				</FORM>
			</td>
		</tr>';
	 $i++; 		
	}
echo '</table>
<br />
<br />';

	?>
<fieldset style="width:170px;margin:auto;">
<legend>Ajouter une catégorie</legend>
<table>
	<FORM method=post action="cate.php">
		<tr style="text-align:center;">
			<td>
					<input type="text" name="new_name_cate"/>
			</td>
		</tr>
		<tr style="text-align:center;">
			<td>
					<INPUT type="submit" value="Cr&eacute;er une categorie">
			</td>	
		</tr>
	</FORM>

</table>
</fieldset>

<?php
include('./includes/corps_bas.php');
?>

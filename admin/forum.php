<?php

include('../includes/fonction.php');

if(!isset($_SESSION['login']) OR $_SESSION['login'] < 2)
{
	header('Location: ../inscription.php');
}else{

$titre = "Gestion des forums";
include('includes/corps_haut.php');


if(isset($_POST['new_name_forum'])){

	$titre=strtolower(preg_replace('#[ ]#', '_', $_POST['new_name_forum']));
	$req = $bdd->prepare('INSERT INTO forums(titre, nom, description, categorie, cache) VALUES(?, ?, ?, ?, ?)');
	$req->execute(array($titre, $_POST['new_name_forum'], $_POST['new_desc_forum'], $_POST['new_cate_forum'], $_POST['new_cache_forum']));

	header('location:forum.php?cree');
	}


if (isset($_POST['titre_modif'])){
	$nom = $_POST['titre_modif'];
	$_POST['titre_modif']=strtolower(preg_replace('#[ ]#', '_', $_POST['titre_modif']));
	
	$req = $bdd->prepare('UPDATE forums SET nom = ?, titre = ?, description = ?, categorie = ?, cache = ? WHERE id = ?');
	$req->execute(array($nom, $_POST['titre_modif'], $_POST['description_modif'], $_POST['categorie_modif'],  $_POST['cache_modif'], $_POST['id_modif']));
	
	$req = $bdd->prepare('UPDATE sujets SET forum = ? WHERE forum = ?');
	$req->execute(array($_POST['titre_modif'], $_POST['titre_forum']));

	$req = $bdd->prepare('UPDATE messages SET forum = ? WHERE forum = ?');
	$req->execute(array($_POST['titre_modif'], $_POST['titre_forum']));
	
	header('location:forum.php?modif');
}

if (isset($_GET['suppr'])){
	echo'<div class="info">Forum supprim&eacute;!</div><br/>';
}
if (isset($_GET['modif'])){
	echo'<div class="info">Forum modifi&eacute;!</div><br/>';
}
if (isset($_GET['cree'])){
	echo'<div class="info">Forum cr&eacute;&eacute;!</div><br/>';
}

if (isset($_POST['modif'])){
	if ($_POST['modif'] == "modifier"){
		$reponse_modif = $bdd->prepare('SELECT * FROM forums where id=?') or die(mysql_error());
		$reponse_modif->execute(array($_POST['id_forum']));

		$donnees_modif = $reponse_modif->fetch();
		
		$reponse_cate = $bdd->query('SELECT * FROM categories') or die(mysql_error());
	?>
		<FORM method=post action="forum.php">
			<table>
				<tr>
					<td>
						<label for="titre">Nom</label>
					</td>
					<td>
						<input type="text" name="titre_modif" id="titre" value="<?php echo $donnees_modif['nom']; ?>"/>
					</td>
				</tr>
				<tr>
					<td>
						<label for="description">Description:</label>
					</td>
					<td>
						<input type="text" name="description_modif" id="description" value="<?php echo $donnees_modif['description']; ?>"/>
					</td>
				</tr>
				<tr>
					<td>
						<label for="categorie">Cat�gorie:</label>
					</td>
					<td>
						<SELECT name="categorie_modif">
					<?php
						while ($donnees_cate = $reponse_cate->fetch()){
							echo '<OPTION VALUE="'.$donnees_cate['titre'].'">'.$donnees_cate['titre'].'</OPTION>';
						}
					?>
						</select>
					</td>
				</tr>
				<tr>
					<td>
						<label for="cache">Cach�:</label>
					</td>
					<td>
						<SELECT name="cache_modif">
					<?php
						if($donnees_modif['cache'] == 0){
							echo '<OPTION selected="selected" VALUE="0">Non cach�</OPTION>
							<OPTION VALUE="1">Cach�</OPTION>';
						}else{
							echo '<OPTION VALUE="0">Non cach�</OPTION>
							<OPTION selected="selected" VALUE="1">Cach�</OPTION>';
						}
					?>
						</select>
					</td>
				</tr>
				<tr style="text-align:center;">
					<td colspan="2">
						<INPUT type="submit" value="Valider">
					</td>
				</tr>
			</table>
			
			<input type="hidden" name="titre_forum" value="<?php echo $donnees_modif['titre'];?>"/>
			<input type="hidden" name="id_modif" value="<?php echo $_POST['id_forum']; ?>"/>
		</FORM><br/>
	<?php
		
	}
	elseif ($_POST['modif'] == "supprimer")
	{
		$reponse = $bdd->prepare('DELETE FROM forums WHERE id = ?');
		$reponse->execute(array($_POST['id_forum']));
		header('Location: forum.php?suppr');   
	}
	else
	{
		echo 'Il y a un soucis, veuillez contacter un administrateur.';
	}
}


?>

<table class="classement">
	<?php
		echo'
		<tr>
			<td>
				Nom 
			</td>
			<td>
				Description 
			</td>
			<td>
				Cat�gorie 
			</td>
		</tr>';	
	$i=1;

	$reponse = $bdd->query('SELECT * FROM forums ORDER BY categorie,id') or die(mysql_error());
	while ($donnees = $reponse->fetch())
	{
		if ($i&1){
			$color = '#ffffff';
		}else{
			$color = '#d2d2d2';
		}
		echo '
		<tr style="background-color:'.$color.';">
			<td>
				'. $donnees['nom'].'
			</td>
			<td>
				'. $donnees['description'].'
			</td>
			<td>
				'. $donnees['categorie'].'
			</td>
			<td>
				<FORM method=post action="forum.php">
					<SELECT name="modif">
						<OPTION VALUE="modifier">Modifier</OPTION>
						<OPTION VALUE="supprimer">Supprimer</OPTION>
					</SELECT>
					<input type="hidden" name="id_forum" value='. $donnees['id'].'/>
					<INPUT type="submit" value="Valider">
				</FORM>
			</td>
		</tr>';
		$i++;		
	}
	?>
</table>

<br/>

<fieldset style="width:250px; margin:auto;">
	<legend>Ajouter un forum</legend>
	
	<table class="bordures">
		<FORM method=post action="forum.php">
			<tr style="text-align:center;">
				<td colspan="2">
						Nom:
				</td>
				<td colspan="2">
						<input type="text" name="new_name_forum">
				</td>
			</tr>
			<tr style="text-align:center;">
				<td colspan="2">
						Description:
				</td>
				<td colspan="2">
						<input type="text" name="new_desc_forum">
				</td>
			</tr>
			<tr style="text-align:center;">
				<td colspan="2">
						Cat�gorie:
				</td>
				<td colspan="2">
					<SELECT name="new_cate_forum">
					<?php
						$reponse_cate = $bdd->query('SELECT * FROM categories') or die(mysql_error());
						while ($donnees_cate = $reponse_cate->fetch()){
							echo '<OPTION VALUE="'.$donnees_cate['titre'].'">'.$donnees_cate['titre'].'</OPTION>';
						}
					?>
					</SELECT>
				</td>
			</tr>
			<tr style="text-align:center;">
				<td colspan="2">
					<label for="cache">Cach�:</label>
				</td>
				<td colspan="2">
					<SELECT name="new_cache_forum">
				<?php
					echo '<OPTION VALUE="0">Non cach�</OPTION>
						<OPTION VALUE="1">Cach�</OPTION>';
				?>
					</select>
				</td>
			</tr>
			<tr style="text-align:center;">
				<td colspan="4">
						<INPUT type="submit" value="Cr&eacute;er un forum">
				</td>	
			</tr>
		</FORM>

	</table>
</fieldset>
<?php
include('includes/corps_bas.php');
}
?>
